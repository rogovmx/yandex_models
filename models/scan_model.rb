class ScanModel < ActiveRecord::Base
  
  belongs_to :product
  
  before_save :check_uniq
  
  protected

  def check_uniq
    ScanModel.where(product_id: self.product_id).limit(1).destroy_all
  end
  
end
