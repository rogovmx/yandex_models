module Ymodel
  class Api
     AUTH = "&oauth_token=c883ee85fe574b8d8d5d2ca62fd9b7e6&oauth_client_id=ac448773268a4ee49e954b09c779f319&oauth_login=eders1978"
    
    def initialize(options = {})

    end

    def perform
      p 'Parsing yandex models...'
      
      @products_for_scan = Product.not_scanned
      ScanModel.destroy_all

      File.open("log/models.log", 'a') {|file| file.write Time.now; file.write " Начало \n" }
      #threads = []
      @products_for_scan.each_slice((@products_for_scan.size / 2) + 1) do |scan_groups|
        scan_groups.each do |scan|
          #threads << Thread.new do
          res = find_product_yandex(scan)
          if find_product_yandex(scan).any?
            ScanModel.create(product_id: scan.id)
            ap res
          end  
          #end
        end
      end  
      #threads.each(&:join)

      File.open("log/models.log", 'a') {|file| file.write Time.now; file.write " Окончание \n \n" }
      p "Parsing done! Scanned: #{Product.scanned.count} from: #{@products_for_scan.size}. #{Time.now}"
      
    end
    
    def test
      File.open("log/test.log", 'a') {|file| file.write Time.now; file.write "\n" }
    end
    
    
    private
    
    def find_product_yandex product
      result = []
      queries = []
      vendor = product.vendor ? product.vendor.name.to_s.strip : ''
      name = product.short_name.to_s.strip.blank? ? product.name.to_s.strip : product.short_name.to_s.strip
      vendor_code = product.vendor_code.to_s.strip
      model = product['model_name'].to_s.strip


      queries << [(vendor + ' ' + name).strip, "Производитель + кор. имя | имя"]
      queries << [(vendor + ' ' + vendor_code).strip, "Производитель + код производителя"]
      queries << [model, "Модель"]
      queries << [vendor_code, "Код производителя"]
      queries << [(name.split(' ').take(3).join(' ')).strip, "кор. имя | имя - 3 слова"]
      queries << [(name.split(' ').take(4).join(' ')).strip, "кор. имя | имя - 4 слова"]
      queries.uniq! { |q| q.first }
      queries.each do |query, descr|
        q = query.gsub(" ", "+")
        unless q.blank?
          resp = get_models_from_yandex q 
          if resp && resp['pager'] && resp['pager']['total'] && resp['pager']['total'] == 1
            result << [q, resp, descr] 
            break
          end
        end
      end
      result.uniq! { |r| r.second['id'] }
      result
    end

    def get_models_from_yandex query
      @url = "https://api.partner.market.yandex.ru/v2/models.json?regionId=2&query=#{URI.encode query}" + AUTH
      
      uri = URI.parse(@url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      resp = response.body
      File.open("log/models.log", 'a') do |file| 
        unless response.code == "200"
          file.write response.code;
          file.write response.body.inspect; 
          file.write Time.now; file.write "\n" 
          ap response.body.inspect
        end  
      end
      JSON.parse(resp)
    rescue => error
      File.open("log/models.log", 'a') do |file| 
          file.write error.inspect 
          file.write Time.now; file.write "\n" 
      end
      ap error.inspect 
      nil
    end
    
    
  end
end