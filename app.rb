require 'active_record'
require 'sqlite3'
require 'mysql2'
require 'logger'
require 'open-uri'
require 'nokogiri'
require "csv"
require 'ap'
require "net/https"
require "uri"

Dir["./lib/*.rb"].each {|file| require file }
Dir["./models/*.rb"].each {|file| require file }


ActiveRecord::Base.logger = Logger.new('log/active_record.log')
config = YAML::load File.read('config/database.yml')
ActiveRecord::Base.establish_connection(config["default"])
WorkBase.establish_connection(config["work_base"])