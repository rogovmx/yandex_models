class CreateTables < ActiveRecord::Migration
  def change
    create_table :scan_models do |t|
      t.integer :product_id

      t.timestamps null: false
    end
  end
end